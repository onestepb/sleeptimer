package com.sleeptimer;

import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.Switch;

public class settings extends AppCompatActivity {
    private DevicePolicyManager devicePolicyManager;
    private ComponentName compName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final SharedPreferences settings = getSharedPreferences("Einstellungen", 0);
        boolean designlight=false;
        if (settings.contains("designlight")) designlight = settings.getBoolean("designlight", false);
        if (designlight) setTheme(R.style.ThemeOverlay_AppCompat_Light);
        else setTheme(R.style.ThemeOverlay_AppCompat_Dark);
        setContentView(R.layout.activity_settings);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        devicePolicyManager = (DevicePolicyManager) getSystemService(DEVICE_POLICY_SERVICE);
        compName = new ComponentName(this, MyAdmin.class);
        Switch rechte = findViewById(R.id.switch1);
        rechte.setOnCheckedChangeListener(change);
        boolean active = devicePolicyManager.isAdminActive(compName);
        rechte.setChecked(active);
        Switch design = findViewById(R.id.theme);
        design.setChecked(designlight);
        design.setOnCheckedChangeListener(designchange);

        final Button ok = findViewById(R.id.okb);
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }



    private final Switch.OnCheckedChangeListener change = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            if (isChecked) {
                Intent intent = new Intent(DevicePolicyManager.ACTION_ADD_DEVICE_ADMIN);
                intent.putExtra(DevicePolicyManager.EXTRA_DEVICE_ADMIN, compName);
                intent.putExtra(DevicePolicyManager.EXTRA_ADD_EXPLANATION, R.string.rechte2);
                startActivityForResult(intent, MainActivity.RESULT_ENABLE);
            } else {
                devicePolicyManager.removeActiveAdmin(compName);
            }
        }
    };

    private final CompoundButton.OnCheckedChangeListener designchange = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
            SharedPreferences settings = getSharedPreferences("Einstellungen", 0);
            SharedPreferences.Editor editor = settings.edit();
            editor.putBoolean("designlight", isChecked);
            editor.apply();
            Intent intent = getIntent();
            finish();
            startActivity(intent);
        }
    };


}
