package com.sleeptimer;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.media.AudioManager;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    public static final int RESULT_ENABLE = 11;
    public static int sekunden, minuten, stunden;
    public boolean dostartstop=false;
    public static CountDownTimer counter;
    public static boolean designlight = false;
    public int originalVolume=-1;



    private final View.OnClickListener resetaction = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            final EditText sekundenEingabe = findViewById(R.id.sekunden);
            final EditText minutenEingabe = findViewById(R.id.minuten);
            final EditText stundenEingabe = findViewById(R.id.stunden);
            Button resetbutton = findViewById(R.id.resetbutton);
            Button startbutton = findViewById(R.id.starttimerbutton);
            resetbutton.setEnabled(false);
            startbutton.setText(R.string.start_text);
            sekundenEingabe.setText("0");
            minutenEingabe.setText("0");
            stundenEingabe.setText("0");
            sekundenEingabe.setEnabled(true);
            minutenEingabe.setEnabled(true);
            stundenEingabe.setEnabled(true);
            counter.cancel();
            counter=null;
            counteractive=false;
            stunden=0;
            minuten=0;
            sekunden=0;
            final NotificationManagerCompat notificationManager = NotificationManagerCompat.from(getApplicationContext());
            notificationManager.cancel(1212);
            if (originalVolume != -1) {
                AudioManager am = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
                am.setStreamVolume(AudioManager.STREAM_MUSIC, originalVolume, AudioManager.FLAG_SHOW_UI);
            }

        }
    };

    public static boolean counteractive = false;
    private DevicePolicyManager devicePolicyManager;
    private ComponentName compName;

    private final View.OnClickListener sleeper = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            boolean active = devicePolicyManager.isAdminActive(compName);
            if (!active) {
                Toast.makeText(getApplicationContext(),R.string.rechte3,Toast.LENGTH_LONG).show();
                return;
            }
            final Button timerstart = findViewById(R.id.starttimerbutton);
            Button resettimer = findViewById(R.id.resetbutton);
            if (timerstart.getText().toString().equals(getResources().getString(R.string.pause_text))) {
                counter.cancel();
                counteractive=false;
                timerstart.setText(R.string.resume_text);
                resettimer.setEnabled(true);
            } else if (timerstart.getText().toString().equals(getResources().getString(R.string.resume_text))) {
                counter.start();
                counteractive=true;
                timerstart.setText(R.string.pause_text);
                resettimer.setEnabled(false);
            } else {
                resettimer.setEnabled(false);
                final EditText sekundenEingabe = findViewById(R.id.sekunden);
                final EditText minutenEingabe = findViewById(R.id.minuten);
                final EditText stundenEingabe = findViewById(R.id.stunden);
                sekunden = Integer.parseInt(sekundenEingabe.getText().toString());
                minuten = Integer.parseInt(minutenEingabe.getText().toString());
                stunden = Integer.parseInt(stundenEingabe.getText().toString());
                minutenEingabe.clearFocus();
                sekundenEingabe.clearFocus();
                sekundenEingabe.clearFocus();
                sekundenEingabe.setEnabled(false);
                minutenEingabe.setEnabled(false);
                stundenEingabe.setEnabled(false);
                timerstart.setText(R.string.pause_text);

                final NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(getApplicationContext(),"1212");
                mBuilder.setSmallIcon(R.mipmap.ic_notification);
                mBuilder.setColor(Color.blue(Color.MAGENTA));
                mBuilder.setBadgeIconType(R.mipmap.ic_launcher);
                mBuilder.setContentTitle(getString(R.string.notifikation));
                mBuilder.setContentText(stundenEingabe.getText()+":"+minutenEingabe.getText()+":"+sekundenEingabe.getText());
                mBuilder.setOnlyAlertOnce(true);
                Intent intent= getIntent();
                PendingIntent notifyPendingIntent = PendingIntent.getActivity(MainActivity.this, 0, intent, PendingIntent.FLAG_IMMUTABLE);
                mBuilder.setContentIntent(notifyPendingIntent);
                final NotificationManagerCompat notificationManager = NotificationManagerCompat.from(getApplicationContext());
                notificationManager.notify(1212, mBuilder.build());
                stundenEingabe.clearFocus();
                timerstart.requestFocus();


                int sleepzeit = sekunden; // sleepzeit=sekunden+minuten+stunden  Einheit: sekunden
                if (stunden > 0) minuten += 60 * stunden;
                if (minuten > 0) sleepzeit += 60 * minuten;
                if (stunden > 0) minuten -= 60 * stunden;
                counteractive=true;
                counter = new CountDownTimer(sleepzeit * 1000, 1000) {
                    public void onTick(long millisUntilFinished) {

                        if (sekunden == 0 & minuten > 0) {
                            sekunden = 60;
                            minuten--;
                            minutenEingabe.setText(String.valueOf(minuten));
                        } else if (sekunden > 0) sekunden--;

                        if (minuten == 0 & stunden > 0) {
                            minuten = 60;
                            stunden--;
                            stundenEingabe.setText(String.valueOf(stunden));
                            minutenEingabe.setText(String.valueOf(minuten));
                        }

                        sekundenEingabe.setText(String.valueOf(sekunden));
                        sekundenEingabe.clearFocus();
                        mBuilder.setContentText(stunden +":"+ minuten +":"+ sekunden);
                        notificationManager.notify(1212, mBuilder.build());

                        if (stunden==0 && minuten==0 && sekunden <= 10) {
                            AudioManager am = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
                            if (originalVolume==-1) originalVolume=am.getStreamVolume(AudioManager.STREAM_MUSIC);
                            am.adjustVolume(AudioManager.ADJUST_LOWER,AudioManager.FLAG_SHOW_UI);
                        }
                    }

                    public void onFinish() {
                        if (!dostartstop){
                            sekundenEingabe.setText("0");
                        boolean active = devicePolicyManager.isAdminActive(compName);
                        counteractive=false;
                        timerstart.setEnabled(true);
                        sekundenEingabe.setEnabled(true);
                        minutenEingabe.setEnabled(true);
                        stundenEingabe.setEnabled(true);
                        timerstart.setText(R.string.start_text);
                        AudioManager.OnAudioFocusChangeListener focusChangeListener =
                                new AudioManager.OnAudioFocusChangeListener() {
                                    public void onAudioFocusChange(int focusChange) {
                                        AudioManager am = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
                                    }
                                };
                        AudioManager am = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
                        // Request audio focus (for standby)
                        int result = am.requestAudioFocus(focusChangeListener, AudioManager.STREAM_MUSIC, AudioManager.AUDIOFOCUS_GAIN);
                        if (active) {
                            devicePolicyManager.lockNow();
                        } else {
                            Toast.makeText(getApplicationContext(), R.string.rechte3, Toast.LENGTH_LONG).show();
                        }
                        am.setStreamVolume(AudioManager.STREAM_MUSIC,originalVolume,AudioManager.FLAG_SHOW_UI);
                    }
                        notificationManager.cancel(1212);
                    }


                }.start();


            }
        }
    };


    private void createNotificationChannel() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = "NAmedeschannels";
            String description = "blabbla";
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel("1212", name, importance);
            channel.setDescription(description);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final SharedPreferences settings = getSharedPreferences("Einstellungen", 0);
        if (settings.contains("designlight")) designlight = settings.getBoolean("designlight", false);
        if (designlight) setTheme(R.style.ThemeOverlay_AppCompat_Light);
        else setTheme(R.style.ThemeOverlay_AppCompat_Dark);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        devicePolicyManager = (DevicePolicyManager) getSystemService(DEVICE_POLICY_SERVICE);
        compName = new ComponentName(this, MyAdmin.class);

        createNotificationChannel();

        Button starttimerbutton, resetbutton;
        starttimerbutton = findViewById(R.id.starttimerbutton);
        resetbutton = findViewById(R.id.resetbutton);
        starttimerbutton.setOnClickListener(sleeper);
        resetbutton.setOnClickListener(resetaction);
        boolean active = devicePolicyManager.isAdminActive(compName);
        if (!active) {
            Intent intent = new Intent(DevicePolicyManager.ACTION_ADD_DEVICE_ADMIN);
            intent.putExtra(DevicePolicyManager.EXTRA_DEVICE_ADMIN, compName);
            intent.putExtra(DevicePolicyManager.EXTRA_ADD_EXPLANATION, R.string.rechte2);
            startActivityForResult(intent, RESULT_ENABLE);
        }


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
                Intent intent = new Intent(MainActivity.this, settings.class);
                startActivity(intent);
            return true;
        }
        if (id == R.id.action_about) {
            Intent intent = new Intent(MainActivity.this, about.class);
            startActivity(intent);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

   @Override
    public void onBackPressed() {
         moveTaskToBack(true);
    }


    @Override
    public void onResume() {
        super.onResume();
        SharedPreferences settings = getSharedPreferences("Einstellungen", 0);
        Button pausetaste = findViewById(R.id.starttimerbutton);
        if (!counteractive && pausetaste.getText().toString().equals(getResources().getString(R.string.pause_text)))
            pausetaste.setText(R.string.resume_text);
        else if (counteractive && pausetaste.getText().toString().equals(getResources().getString(R.string.resume_text)))
            pausetaste.setText(R.string.pause_text);
        if (settings.contains("designlight")) {
            if (!(settings.getBoolean("designlight", true) == designlight)) {
                Intent intent = getIntent();
                finish();
                startActivity(intent);
            }
        }

    }

}
