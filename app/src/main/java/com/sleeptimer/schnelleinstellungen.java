package com.sleeptimer;

import android.content.Intent;
import android.graphics.drawable.Icon;
import android.service.quicksettings.Tile;
import android.service.quicksettings.TileService;


public class schnelleinstellungen extends TileService {

    public void onClick(){
        //super.onClick();
        Tile tile = getQsTile();
        tile.setIcon(Icon.createWithResource(getApplicationContext(),R.mipmap.ic_notification));
        if (MainActivity.stunden==0 && MainActivity.minuten==0 && MainActivity.sekunden==0) {
            Intent intent = new Intent(this, MainActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivityAndCollapse(intent);
        } else if (!MainActivity.counteractive){
            MainActivity.counter.start();
            MainActivity.counteractive=true;
            tile.setState(Tile.STATE_ACTIVE);
            tile.updateTile();
        } else {
            MainActivity.counter.cancel();
            MainActivity.counteractive=false;
            tile.setState(Tile.STATE_INACTIVE);
            tile.updateTile();
        }

    }

    public  void onStartListening(){
        Tile tile = getQsTile();
        if ( MainActivity.counteractive)  {
            tile.setState(Tile.STATE_ACTIVE);
        }
        else  {
            tile.setState(Tile.STATE_INACTIVE);
        }
        tile.updateTile();
    }


}
