SleepTimer
------------

An app for Android, that activates the standby mode after a specified time, even if the system-standby is disabled for example while streaming a video.

[<img src="https://f-droid.org/badge/get-it-on.png"
     alt="Get it on F-Droid"
     height="80">](https://f-droid.org/packages/com.sleeptimer/)
